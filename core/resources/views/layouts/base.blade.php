<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
	<title>@yield('title')</title>
	@include('layouts.css')
</head>

<body>
	<div>
		@include('layouts.header')

		<section>
			@yield('content')
		</section>

		@include('layouts.footer')
	</div>
	@include('layouts.js')
</body>

</html>