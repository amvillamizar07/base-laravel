<!-- jQuery 3.2.1 -->
{!!Html::script('bookstores\node_modules\jquery\dist\jquery.min.js')!!}

{{--  juqery ui  --}}
{!!Html::script('bookstores\plugins\jquery-ui-1.12.1\jquery-ui.min.js')!!}

{{--  sweetalert 2  --}}
{!!Html::script('bookstores\node_modules\sweetalert2\dist\sweetalert2.min.js')!!}

{{--  Fontaweso free 5.0.2  --}}
{!!Html::script('bookstores\plugins\fontawesome-free-5.0.2\svg-with-js\js\fontawesome-all.min.js')!!}

{{--  popper 1.12.3 para usar bootstrap --}}
{!!Html::script('bookstores\node_modules\bootstrap\dist\js\popper.min.js')!!}

<!-- Bootstrap 4.0.0-beta.2 -->
{!!Html::script('bookstores\node_modules\bootstrap\dist\js\bootstrap.min.js')!!}

{{--  manejo de funciones personales  --}}
{!!Html::script('bookstores\dist\js\funtions.js')!!}









