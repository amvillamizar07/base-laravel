<footer>
	<div class="container">
		<div class="pull-right hidden-xs">
			<b>Version</b> 1.0
		</div>
		<strong>Copyright &copy; 2017-2018
			<a href="https://thelastsoft.com">The last Software</a>.</strong> Todos los derechos reservados.
	</div>
</footer>