<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

{{--  Bootstrap 4.0.0-beta.2  --}}
{!! Html::style('bookstores/node_modules/bootstrap/dist/css/bootstrap.min.css') !!}

<!-- Google Font -->
{!!Html::style('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic')!!}

{{--  css personalizado  --}}
{!!Html::style('bookstores/dist/css/style.css')!!}